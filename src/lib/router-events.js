
import mitt from 'mitt';
import Router from 'next/router';

const emitter = mitt();
export default emitter;

Router.onRouteChangeStart = (...args) => {
  console.log("onRouteChangeStart start");
  emitter.emit('routeChangeStart', ...args);
};

Router.onRouteChangeComplete = (...args) => {
  console.log("routeChangeComplete done");
  emitter.emit('routeChangeComplete', ...args);
};

Router.onRouteChangeError = (...args) => {
  console.log("routeChangeError done");
  emitter.emit('routeChangeError', ...args);
};