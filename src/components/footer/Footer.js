const Footer = () => (
  <footer>
    <div className="copyright">
      Copyright © 2019 - Alephart Co, Web & Mobile, All Right Reserved 
    </div>
    <style jsx>{`
      footer {
        grid-area: footer;
        background-color: #615AE6;
        color: #ffffff;       
        border-top: 1px solid #7A83F4;
        display: flex;
        justify-content: center;
        align-items: center;
      }

      .copyright {
        text-align: center;
      }

      @media(max-width: 480px) {
        .copyright {
          font-size: 0.7rem;
        }
      }

    `}</style>
  </footer>
)

export default Footer