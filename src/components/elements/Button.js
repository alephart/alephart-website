import React from 'react'
import styled, { css } from 'styled-components'

const StyleButton = styled.a`
  border: none;     
  border-radius: ${props => (props.radius || '50px')};
  padding: 10px 20px;
  margin: 10px 10px;
  display: inline-block;
  color: #ffffff;
  background-color: ${props => (props.secondary ? '#615AE6' : '#FF82A0')};
  font-size: ${props => (props.big ? '1em' : '0.8em')};
  line-height: 1.1;
  font-weight: 400;
  letter-spacing: 0.05em;
  text-align: center;
  text-decoration: none;
  text-transform: uppercase;
  box-shadow: 0 0 0 0 rgba(0,0,0,0.2);
  transition: all .3s;
  cursor: pointer;

  &:hover {
    color: #ffffff;
    background-color: ${props => (props.secondary ? '#7A83F4' : '#f56789')};
    box-shadow: 0 4px 6px 0 rgba(0,0,0,0.2);
  }
`;

const Button = ({ children, ...props }) => {
  return (
    <StyleButton {...props}>
      {children}
    </StyleButton>
  )
}

/**
 * Implement React.forwardRef for reference warning
 * Warning: Function components cannot be given refs. Attempts to access this ref will fail. Did you mean to use React.forwardRef()?
 */

const ButtonLink = React.forwardRef((props, ref) => {
  return (
    <StyleButton href={ref} {...props}>
      {props.children}
    </StyleButton>
  )
})

export default Button
export { ButtonLink }