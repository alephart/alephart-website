import Link from 'next/link'
import styled from 'styled-components'

const Items = styled.ul`
    list-style: none;
    padding: 0;
    margin: 10px auto;
    display: flex;
    justify-content: center;
`;

const Item = styled.li`
    & :first-child {
      list-style: none;
    }

    margin: 5px 2px;
    padding: 5px 3px;
    color: ${ props => props.theme.colors.redsoft }; 

    @media screen and (min-width: 768px){
      padding: 5px 10px;    
    }

    a {
      font-size: 1.2em;
    }
`;

const ApproachItem = ({item}) => (
  <Item>
    <Link href="/">
      <a>{ item }</a>
    </Link>
  </Item>
)

const Approach = ({ items, ...props }) => {
  return (
    <Items items={items} {...props}>
      {items.map((item, index) => (
        <ApproachItem item={item} key={item+index} />
      ))}
    </Items>
  )
}

export default Approach