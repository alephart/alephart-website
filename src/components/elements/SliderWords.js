import styled, { css, keyframes } from "styled-components";

/***
 * Next function generates de keyframes according to words quantity.
 * Now with styled-components, excelent!.
***/
const DataKeyframes = (words) => {
  const steps = [...Array(words.length)].map((_, i) => Math.floor((i * 100) / words.length));
  let tempkf = '';
  
  for (let index = 0; index < words.length-1; index++) {
    let next = index+1;
    let step = Math.floor(100 / (words.length-1));
    let animationStep = Math.floor(step * .3);

    tempkf = tempkf + `
      ${step * index}%, 
      ${(step * next) - animationStep}% {
        -webkit-transform: translateY(-${ steps[index]}%);
                transform: translateY(-${ steps[index]}%);
      }
    `;
  }

  return tempkf + `
    100% {
      -webkit-transform: translateY(-${steps[words.length - 1]}%);
              transform: translateY(-${steps[words.length - 1]}%);
    }
  `;
};

const AnimSlider = (words) => keyframes`
  ${ DataKeyframes(words) }
`;

/*** 
 * Styles
***/

const Wrap = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
`;

const Content = styled.div`
  display: inline-block;
  overflow: hidden;
  vertical-align: middle;
  height: 4.5em;
  mask-image: linear-gradient(transparent, white, white, white, transparent);
  mask-type: luminance;
  mask-mode: alpha;
`;

const Words = styled.ul`
  list-style: none;
  display: inline-block;
  margin: 0;
  padding: 0;
  animation: ${props => AnimSlider(props.words)} infinite 12s;
  animation-timing-function: ease-out;
`;

const Word = styled.li`
  display: block;
  line-height: 1.4em;
  text-align: center;
  font-size: 2.66em;
  padding: 0 20px;
  font-weight: 500;
  color: #FF82A0;
`;

const SliderWords = ({ words }) => {
  return (
    <Wrap>
      <Content>
        <Words words={ words }>
          {words.map((word, index) => (
            <Word key={word + index}>
              {word}
            </Word>
          ))}
        </Words>
      </Content>
    </Wrap>
  )
}

export default SliderWords