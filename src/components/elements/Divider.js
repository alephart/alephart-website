import styled, { css } from 'styled-components'

const StyleDivider = styled.div`
  line-height: 0;
  font-size: 0;

  @media screen and (max-width: 767px){  
    padding-top: ${props => props.spacingMobile || '1em' };
    padding-bottom: ${props => props.spacingMobile || '1em' };
  }

  @media screen and (min-width: 768px){  
    padding-top: ${props => props.spacingDesktop || '1em' };
    padding-bottom: ${props => props.spacingDesktop || '1em' };
  }

`;

const Divider = ({ spacingDesktop, spacingMobile, ...props }) => {
  return (
    <StyleDivider spacingDesktop={spacingDesktop} spacingMobile={spacingMobile} {...props} />
  )
}
  
export default Divider