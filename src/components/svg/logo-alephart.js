import React from 'react';

export default () => (
  <React.Fragment>
    <div className="logo">
      <svg
        xmlns="http://www.w3.org/2000/svg"
        xlink="http://www.w3.org/1999/xlink"
        width="100%"
        height="100%"
        viewBox="0 0 338.321 64.869">
        <defs>
          <linearGradient id="a" x1="0.5" y1="0.006" x2="0.5" y2="0.997" gradientUnits="objectBoundingBox">
            <stop offset="0" stopColor="#56b4f2" />
            <stop offset="1" stopColor="#615ae6" />
          </linearGradient>
        </defs>
        <g transform="translate(-121.808 -112.284)">
          <g transform="translate(121.808 112.284)">
            <circle cx="32.435" cy="32.435" r="32.435" fill="url(#a)" />
            <g transform="translate(11.858 10.72)">
              <path d="M187.3,166.9c-4.374-9.2-35.253-19.639-38.181-26.288-1.333-3.026-4.046,10.032-.942,13.194,6.809,6.935,26.135,10.288,36.286,24.351C186.162,180.506,189.186,170.861,187.3,166.9Z" transform="translate(-146.746 -137.623)" fill="#fff" />
              <path d="M157.249,196.219c-.656-.484-.433,5.466,2.046,5.145,12.245-1.585,20.211,1.786,20.069.78-.6-4.211-17.13-10.752-21.921-15.576-.85-.856-.449,1.977.077,3.409a34.39,34.39,0,0,0,3.491,6.659A4.046,4.046,0,0,1,157.249,196.219Z" transform="translate(-152.088 -161.882)" fill="#fff" />
              <path d="M197.739,151.8c1.189,1.4,2.94-5.2,2.37-6.942-1.492-4.564-13.7-6.375-15.745-9.809-.844-1.419-2.977,4.5-2.539,6.095C183.138,145.924,194.295,147.756,197.739,151.8Z" transform="translate(-165.113 -134.828)" fill="#fff" />
            </g>
          </g>
          <text transform="translate(199.129 159.211)" fill="#615ae6" fontSize="47" fontFamily="aller_regular">
            <tspan x="0" y="0">Alephart Co.</tspan>
          </text>
        </g>
        </svg>
    </div>
    <style jsx>{`
      .logo {
        width: 180px;
        height: 34.6px;
      }
    `}</style>
  </React.Fragment>
);