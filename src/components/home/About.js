import Link from 'next/link'
import styled from 'styled-components'
import Approach from '../elements/Approach'
import SliderWords from '../elements/SliderWords'
import Button, { ButtonLink } from '../elements/Button'
import Divider from '../elements/Divider'

const focus = [
  'Emprendedores',
  'MyPymes',
  'Empresas',
  'Agencias'
];

/** repite first and last word, late effect   */
const words = [
  'Negocios',
  'Tienda',
  'Catálogo',
  'Portafolio',
  'Comunicación',
  'Servicios',
  'Productos',
  'Juegos',
  'Corporativa',
  'Negocios'
];

const AboutArticle = styled.div`
  background: url('/svg/forma-aco-1.svg') no-repeat center center;
  background-size: 80%;
  display: grid;
  
  @media screen and (min-width: 768px) {
    background-size: 60%;
  }

  article {
    text-align: center;
    width: 360px;
    margin: 20px auto;
    padding-left: 5%;
    padding-right: 5%;    
  }

  @media screen and (min-width: 768px) {
    article {
      width: 600px;
      margin: 80px auto;
      padding-left: 0;
      padding-right: 0;
    }
  }
`;

const About = () => (
  <AboutArticle>
    <article>
      <h1>Somos un proveedor de tecnología web</h1>
      <p>Nuestros clientes son personas que requieren soluciones de software especificas para web y mobile, con acompañamiento constante para optimizar sus procesos.</p>
      
      <Approach items={ focus } />
      
      <h4 style={{ margin: 0 }}>Inicia tu emprendimiento con una App de</h4>
      <SliderWords words={ words } />
      <Divider spacingDesktop='2em' spacingMobile='0' />       
      <Link href="mailto:info@alephart.co" passHref>
        <ButtonLink secondary big>Contacta ya!</ButtonLink>
      </Link>
    </article>
  </AboutArticle>
)    

  export default About