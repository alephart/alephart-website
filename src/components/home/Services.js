import Link from 'next/link'
import styled from 'styled-components'
import ImgWebsite from '../svg/service-website'
import ImgApp from '../svg/service-app'
import ImgPWA from '../svg/service-pwa'
import ImgTeam from '../svg/service-team'
import ImgInfrast from '../svg/service-infrastructure'
import { ButtonLink } from '../elements/Button'

const ServicesArticle = styled.div`
  background: rgba(255, 130, 160, .1);
  padding: 50px 0;
  margin: 0 auto;
  border-top: 1px solid #EAEAEA;
                                                                                   
  article {
    margin: 20px auto;
    padding-left: 5%;
    padding-right: 5%;
  }

  h2 {
    text-align: center;
  }

  h3 {
    color: ${ props => props.theme.colors.redsoft };
    font-size: 1.6em;
  }

  blockquote {
    position: relative;
    font-weight: 300;
    color: #707070;
    width: 300px;
    padding: 10px 20px;
    border-top: 1px solid;
    border-bottom: 1px solid;
    margin-bottom: 40px;
    margin-left: 0;
  }

  blockquote:before {
    content: "“";
    position: absolute;
    top: -20px;
    left: 0;
    font-size: 2em;
    color: #707070;
  }

  blockquote:after {
    content: "”";
    position: absolute;
    font-size: 2em;
    bottom: -20px;
    right: 0;
  }

  blockquote footer {
    position: absolute;
    bottom: -20px;
    right: 0;
    font-style: italic;
  }

  .service {
    display: flex;
    flex-direction: column;
    position: relative;
    padding-bottom: 2.5rem;
  }

  .service:before {
    content: '';
    display: inline-block;
    position: absolute;
    top: 0;
    left: 0;
    height: 15px;
    width: 15px;
    border-radius: 50%;
    background: #FF82A0;
    z-index: 1;
  }

  .service:after {
    content: '';
    border-left: 2px dashed #c7c7c7;
    height: 100%;
    position: absolute;
    top: 0;
    left: 7px;
    z-index: 0;
  }

  .init {
    height: 100px;
  }

  .final {
    margin-top: -10px;
    text-align: center;
  }

  .smart:before {
    height: 30px;
    width: 30px;
    top: 50%;
    left: -7px;
    padding: 5px;
    color: #fff;
    text-align: center;
    font-size: 16px;
  }

  .smart-s:before {
    content: 'S';
  }

  .smart-m:before {
    content: 'M';
  }

  .smart-a:before {
    content: 'A';
  }

  .smart-r:before {
    content: 'R';
  }

  .smart-t:before {
    content: 'T';
  }

  .side-one, 
  .side-two {
    flex: 100%;
    max-width: 100%;
  }

  .side-one {
    margin: 20px;
    padding: 20px;
  }

  .side-two {
    margin: 20px;
    padding: 0 40px;
  }

  .image {
    width: 280px;
    margin: 0 auto;
  }

  @media(min-width: 768px) {

    blockquote {
    width: 100%;
  }

    .service {
      flex-direction: row;
    }

    .service:before {
      left: 50%;
    }

    .service:after {
      left: calc(50% + 7px);
    }

    .smart:before {
      left: calc(50% - 7px);
    }

    .smart-m  .side-one,
    .smart-r  .side-one {
      order: 1;
    }

    .side-one, 
    .side-two {
      flex: 50%;
      max-width: 50%;
    }    
  }

  @media(min-width: 1280px) {
    .image {
      width: 420px;
    }
  }
`;

const Services = () => (
  <ServicesArticle>
    <article>
    
      <h2>Nuestros Servicios</h2>
      <div className="service init"></div>

      <div className="service smart smart-s">
        <div className="side-one">
          <div className="image">
            <ImgWebsite />
          </div>
        </div>
      
        <div className="side-two">
          <h3>Website</h3>
          <blockquote>Un Website es tu presentación en línea. <footer>&mdash;alephart.co</footer></blockquote>
          <p>
          Creamos, conceptualizamos y desarrollamos sitios y páginas web a tu medida, innovadoras y realmente funcionales, con características SEO y Mobile Frienly.</p>
          <ul>
            <li>Página Web</li>
            <li>Sitio Web</li>      
            <li>Tienda en línea</li>
            <li>Landing page</li>
            <li>CMS</li>
            <li>Diseño Responsive</li>
          </ul>  
        </div>
      </div>

      <div className="service smart smart-m">
        <div className="side-one">
          <div className="image">
            <ImgApp />
          </div>
        </div>

        <div className="side-two">
        <h3>App Mobile</h3>
        <blockquote>El mundo es mobile, se parte de esto. <footer>&mdash;alephart.co</footer></blockquote>
        <p>Te apoyamos en la construcción de Apps para móviles, llevamos tu visión a una realidad, trabajamos desde la idea inicial a prototipos y el resultado final.</p>
        <ul>
            <li>Apps Android</li>
            <li>Apps iOS</li>
            <li>Apps Hibridas</li>
            <li>Prototipo</li>
            <li>UX/UI</li>
          </ul>  
        </div>
      </div>
      
      <div className="service smart smart-a">
        <div className="side-one">
          <div className="image">
            <ImgPWA />
          </div>
        </div>
      
        <div className="side-two">
          <h3>App Web</h3>
          <blockquote>Administra tus soluciones Web de forma efectiva. <footer>&mdash;alephart.co</footer></blockquote>
          <p>Una App Web te permite gestionar tus servicios y productos, e incluso tener una mayor efectividad y reducción de costos en tus soluciones digitales.</p>
          <ul>
            <li>Plataformas</li>
            <li>PWA</li>
            <li>Diseño Adaptive</li>
            <li>Infraestructura</li>
            <li>No más Apps Stores</li>
          </ul>  
        </div>
      </div>
      
      <div className="service smart smart-r">
        <div className="side-one">
          <div className="image">
            <ImgInfrast />
          </div>
        </div>
      
        <div className="side-two">
          <h3>Web Infrastructure</h3>
          <blockquote>Todo lo necesario para un Web Moderno. <footer>&mdash;zeit.co</footer></blockquote>
          <p>Trabajamos en partner con compañías especializadas en Cloud Computing, nos encargamos de todo el manejo de infraestructura mientras tu desarrollas tu negocio.</p>
          <ul>
            <li>Hosting</li>
            <li>Cloud Computing</li>
            <li>Serveless</li>
            <li>Git Control</li>
            <li>CI/CD</li>
          </ul>  
        </div>
      </div>
      
      <div className="service smart smart-t">
        <div className="side-one">
          <div className="image">
            <ImgTeam />
          </div>
        </div>
      
        <div className="side-two">
          <h3>Team Training</h3>
          <blockquote>Permítenos apoyarte en la transformación de tu negocio. <footer>&mdash;alephart.co</footer></blockquote>
          <p>Un equipo con conocimientos adecuados en transformación digital, apuntará siempre hacia el mismo objetivo y avanzará más seguro.</p>
          <ul>
            <li>Smart</li>
            <li>Agile</li>
            <li>Scrum</li>
            <li>MVP</li>
            <li>DevOps</li>
            <li>FullStack (FrontEnd - BackEnd)</li>
          </ul>  
        </div>
      </div>

      <div className="final">
        <Link href="mailto:info@alephart.co" passHref>
          <ButtonLink primary>Tu Proyecto Ahora!</ButtonLink>
        </Link>
      </div>

    </article>
  </ServicesArticle>
)

export default Services