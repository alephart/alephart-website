import styled from 'styled-components'

const HowArticle = styled.div`
  background: #FCFCFC;
  padding: 50px 0;
  margin: 0 auto;
  border-top: 1px solid #EAEAEA;

  article {
    margin: 20px auto;
    padding-left: 5%;
    padding-right: 5%;
    text-align: center;
  }

  .items {
    margin: 60px auto;
    display: flex;
    flex-wrap: wrap;
    justify-content: center;

  }

  .item {
    flex: 100%;
    max-width: 100%;
    padding: 20px 40px;
  }

  .item .icon {
    text-align: center;
    width: 80px;
    margin: 0 auto;
  }

  .item img {
    width: 100%;
  }

  @media(min-width: 768px) {
    .item {
      flex: 33%;
      max-width: 33%;
    }

    .item .icon {
      width: 120px;
    }
  } 

  @media(min-width: 920px) {
    .item .icon {
      width: 160px;
    }
  }

  h4 {
    color: ${ props => props.theme.colors.bluelight };
  }
`;

const HowDo = () => (
  <HowArticle>
    <article>
      <h2>¡Despega tu emprendimiento, <br />
      nosotros te apoyamos!</h2>
      <h3>¡Cómo lo hacemos?</h3>
  
      <div className="items">
        <div className="item">
          <div className="icon">
            <img src="/images/icon_metas_inteligentes.png" alt="Metas Inteligentes" />
          </div>
          <h6>Metas Inteligentes</h6>
          <h4>Definir Objetivos Especificos</h4>
          <p>Trabajando en llave con nuestros clientes, definimos metas y objetivos inteligentes, detallando y concretando, estableciendo objetivos comerciales puntuales.</p>
        </div>

        <div className="item">
          <div className="icon">
            <img src="/images/icon_diseno_tecnologia.png" alt="Diseño y Tecnología" />
          </div>
          <h6>Diseño y Tecnología</h6>
          <h4>Marcos de Trabajo Efectivos</h4>
          <p>Ponemos en práctica conceptos y criterios de trabajo probados, para enfocar la problemática planteada, enfocados en el trabajo en equipo y la cultura colaborativa.</p>
        </div>

        <div className="item">
          <div className="icon">
            <img src="/images/icon_medicion_exito.png" alt="Medición del Éxito" />
          </div>
          <h6>Medición del Éxito</h6>
          <h4>Seguimiento de Resultados</h4>
          <p>Medición de resultados desde dos enfoques: Son sus objetivos susceptibles de ser medidos? y, cómo logramos saber si realmente tenemos éxito?.</p>     
        </div>
      </div>    
    
    </article>
  </HowArticle>
)

export default HowDo