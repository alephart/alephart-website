import React from 'react'
import NavBar from './NavBar'

const Header = () => (
  <header>
    <NavBar />
    <style jsx>{`
      header {
        grid-area: header;
        background-color: #F6F6F6;
        border-bottom: 1px solid #E1E1E1;
        display: flex;
        align-self: center;
        justify-content: center;
      }
    `}</style>
  </header>
);

export default Header