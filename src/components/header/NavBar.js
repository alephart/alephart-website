import Link from 'next/link'
import Logo from '../svg/logo-alephart'
import { ButtonLink } from '../elements/Button'

const NavBar = () => (
  <nav>
    <Logo />

    <ul className="nav-links">
      <li>
        <Link href="mailto:info@alephart.co" passHref>
          <ButtonLink>Tu Proyecto Ahora!</ButtonLink>  
        </Link>
      </li>
    </ul>

    <style jsx>{`
      nav {
        display: flex;
        justify-content: space-between;
        align-items: center;
        width: 100%;
        height: 80px;
        padding: 20px;
      }

      .nav-links {
        list-style: none;
        display: flex;
        justify-content: center;
        align-items: center;
      }

      .menu a {
        padding: 10px;
        text-transform: uppercase;
        text-decoration: none;
        font-size: .8rem;
        color: #615AE6;
      }

      .menu a:hover {
        color: #FF82A0;
      }

    `}</style>  
  </nav>
)

export default NavBar