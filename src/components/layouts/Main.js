import React from "react"
import GlobalStyle, { SiteWrapper, MainWrapper } from '../styles/GlobalStyle'
import Meta from '../header/Meta'
import Header from '../header/Header'
import Footer from '../footer/Footer'
import NProgress from '../nprogress'

const MainLayout = ({ children }) => (
    <SiteWrapper>
      <GlobalStyle whiteColor />
      <Meta />
      <NProgress />
      <Header />
      <MainWrapper>
        {children}
      </MainWrapper>
      <Footer />
    </SiteWrapper>
)

export default MainLayout