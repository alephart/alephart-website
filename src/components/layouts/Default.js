import styled from 'styled-components'

const DefaultContainer = styled.div`
  padding: 40px 0;
  margin: 0 auto;
`;

const DefaultLayout = ({ children }) => (
  <DefaultContainer>
    {children}
  </DefaultContainer>
);

export default DefaultLayout;