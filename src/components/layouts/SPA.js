import Meta from './Meta'
import Header from './Header'
import Footer from './Footer'

const Page = props => (
  <div className="layout">
    <Meta />
    <Header />
    <main>
        {props.children}
    </main>
    <Footer />
    <style jsx global>{`

      @font-face {
          font-family: 'aller_bold';
          src: url('static/fonts/aller_bd-webfont.woff2') format('woff2'),
              url('static/fonts/aller_bd-webfont.woff') format('woff');
          font-weight: normal;
          font-style: normal;
      }
  
      @font-face {
          font-family: 'aller_light';
          src: url('static/fonts/aller_lt-webfont.woff2') format('woff2'),
              url('static/fonts/aller_lt-webfont.woff') format('woff');
          font-weight: normal;
          font-style: normal;
      }
      
      @font-face {
          font-family: 'aller_regular';
          src: url('static/fonts/aller_rg-webfont.woff2') format('woff2'),
              url('static/fonts/aller_rg-webfont.woff') format('woff');
          font-weight: normal;
          font-style: normal;
      }

      *, 
      *.before, 
      *.after {
        box-sizing: border-box;
      }

      html, 
      body,
      #__next,
      .layout {
        width: 100%;
        height: 100%;
      }

      body {
        margin: 0;
        padding: 0;
        font-family: 'Ubuntu', sans-serif;
        font-size: 13px;
      }

      @media(min-width: 768px) {
        body {
          font-size: 16px;
        }
      }

      h1, h2, h3, h4, h5, h6 {
        font-weight: 500;        
      }

      h1 {
        font-size: 3em;
      }

      h2 {
        font-size: 2.5em;
      }

      h3 {
        font-size: 2em;
      }

      h4 {
        font-size: 1.8em;
      }

      h5 {
        font-size: 1.4em;
      }
      
      h6 {
        font-size: 1.0em;
      }      

      a {
        text-decoration: none;
        transition: all .2s;
      }

      .layout {
        display: grid;
        grid-template-rows: 80px 1fr 80px;
        grid-template-areas: "header" "main" "footer";
        grid-row-gap: 0;
      }
      
      main {
        grid-area: main;
        align-self: start;
        display: grid;
        min-height: 100%;
      }
      
      @media screen and (max-width: 480px) {
        h1 {
          font-size: 1.2rem;
        }

        .btn {
          padding: 10px;
          margin: 0;
          font-size: 0.6rem;
        }

      }
    `}</style>
  </div>
)

export default Page