import styled, { createGlobalStyle } from 'styled-components'  

const colors = {
  bluesoft:  '#615AE6',
  bluelight: '#7A83F4',
  redsoft:   '#f56789',
  redlight:  '#FF82A0',
  graysoft:  '#E1E1E1',
  graylight: '#F6F6F6',
};

const theme = {
  maxWidthLarge: '1140px',
  maxWidthMedium: '360px',
  maxWidthMini: '300px',
  bs: '0 12px 24px 0 rgba(0, 0, 0, 0.09)',
  colors: colors 
};

const GlobalStyle = createGlobalStyle`
  @font-face {
    font-family: 'aller_bold';
    src: url('/fonts/aller_bd-webfont.woff2') format('woff2'),
        url('/fonts/aller_bd-webfont.woff') format('woff');
    font-weight: normal;
    font-style: normal;
  }

  @font-face {
    font-family: 'aller_light';
    src: url('/fonts/aller_lt-webfont.woff2') format('woff2'),
        url('/fonts/aller_lt-webfont.woff') format('woff');
    font-weight: normal;
    font-style: normal;
  }

  @font-face {
    font-family: 'aller_regular';
    src: url('/fonts/aller_rg-webfont.woff2') format('woff2'),
        url('/fonts/aller_rg-webfont.woff') format('woff');
    font-weight: normal;
    font-style: normal;
  }

  *, 
  *:before, 
  *:after {
    box-sizing: border-box;
  }

  html, 
  body,
  #__next {
    width: 100%;
    height: 100%;
  }

  body {
    margin: 0;
    padding: 0;
    font-family: 'Ubuntu', sans-serif;
    font-size: 13px;
    background: #ffffff;
  }

  @media(min-width: 768px) {
    body {
      font-size: 16px;
    }
  }

  h1, h2 {
    font-weight: 500;        
  }

  h1 { font-size: 3em; }
  h2 { font-size: 2.5em; }
  h3 { font-size: 2em; font-weight: 300; }
  h4 { font-size: 1.8em; font-weight: 300; }
  h5 { font-size: 1.4em; color: ${ colors.bluelight }; margin: 1.4em 0; }
  h6 { font-size: 1.2em; color: ${ colors.redlight }; margin: 1.2em 0; }

  a {
    text-decoration: none;
    color: ${ colors.bluesoft };
    transition: all .2s;
  }

  a:hover {
    color: ${ colors.bluelight };
  }

  p {
    font-size: 1.0em;
    min-height: 1.4em;
    color: #505050;
    font-weight: 300;
  }

  .content {
    width: ${ theme.maxWidthMini };
  }

  @media(min-width: 768px) {
    .content {
      width: ${ theme.maxWidthMedium };
    }    
  }

  @media(min-width: 920px) {
    .content {
      width: ${ theme.maxWidthLarge };
    }    
  }
`;

const SiteWrapper = styled.div`
  width: 100%;
  height: 100%;
  display: grid;
  grid-template-rows: 80px 1fr 80px;
  grid-template-areas: "header" "main" "footer";
  grid-row-gap: 0;
`;

const MainWrapper = styled.main`
  grid-area: main;
`;

export default GlobalStyle;
export {
  theme,
  SiteWrapper,
  MainWrapper
};
