const colors = {
  bluesoft:  '#615AE6',
  bluelight: '#7A83F4',
  redsoft:   '#f56789',
  redlight:  '#FF82A0',
  graysoft:  '#E1E1E1',
  graylight: '#F6F6F6',
};

const themeGlobal = {
  maxWidthLarge: '1000px',
  maxWidthMedium: '400px',
  maxWidthMini: '320px',
  bs: '0 12px 24px 0 rgba(0, 0, 0, 0.09)',
  colors: { colors }
};

const themeLight = {};

const themeNight = {};