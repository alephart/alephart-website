import React from 'react'
import App from 'next/app'
import { ThemeProvider } from "styled-components"
import { theme } from '../components/styles/GlobalStyle'
import MainLayout from '../components/layouts/Main'
import DefaultLayout from '../components/layouts/Default'

class MyApp extends App {
  render () {
    const { Component, pageProps } = this.props
    const Layout = Component.Layout || DefaultLayout

    return (
      <ThemeProvider theme={theme}>
        <MainLayout>
          <Layout>
            <Component {...pageProps} />
          </Layout>
        </MainLayout>
      </ThemeProvider>
    )
  }
}

export default MyApp