import React from 'react'
import About from '../components/home/About'
import HowDo from '../components/home/HowDo'
import Services from '../components/home/Services'

const Index = () => (
  <div>
    <About />
    <HowDo />
    <Services />
  </div>
)

export default Index